#!/bin/sh
#######################################################################
# Copyright (c): 2022-2022, XFusion Tech. Co., Ltd.
# descript: install opengauss with dialog
#           
# version:  1.0
# date:     2022-07-15
#######################################################################
# user should not use this script directly, this script should called by visual_install.sh
config_file_name=$1
database_passwd=$2
cd $(dirname $0)
source ../../_openGauss_profile/openGauss_install_profile
rm -rf _visual_install_output_file
echo "--gsinit-parameter=--pwpasswd=$database_passwd" | python3 -u ./gs_install -X $config_file_name >_visual_install_output_file 2>&1 &
install_pid=$!
echo $install_pid >_install_pid
wait $install_pid
result=$?
echo $result > _install_result