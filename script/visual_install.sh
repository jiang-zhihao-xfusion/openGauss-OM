#!/bin/sh
#######################################################################
# Copyright (c): 2022-2022, XFusion Tech. Co., Ltd.
# descript: install opengauss with dialog
#           
# version:  1.0
# date:     2022-07-15
#######################################################################
# This is a GUI tool for installing openGauss,
# this script will simplified user's operation when installing openGauss.
# You need install dialog before useing this tool.
# "sh visual_install.sh" to start this tool
function check_dialog_res()
{
    result=$?
    if [ $result == 1 ]; then
        exit
    fi
}

function gen_config_file()
{
    cd $evn_file_path
    i=1
    while true
    do
        if [ ! -f "cluster_$i.xml" ]
        then
            break
        fi
        i=$((i+1))
    done
    option_file_name=$evn_file_path/cluster_$i.xml
    cd $script_path

    dialog --title "$title_str" --ok-label "下一步" --cancel-label "取消" --form "设置配置文件选项" 15 45 6 \
    "节点数" 1 1 "" 1 10 25 0 \
    "安装路径" 2 1 "" 2 10 25 0 \
    "DN路径" 3 1 "" 3 10 25 0 \
    "DN端口" 4 1 "" 4 10 25 0 \
    "CM端口" 5 1 "" 5 10 25 0 2> $install_temp_file

   check_dialog_res
    
    nodeCount=$(head -1 $install_temp_file)
    install_path=$(sed -n '2p' $install_temp_file)
    DN_path=$(sed -n '3p' $install_temp_file)
    DN_port=$(sed -n '4p' $install_temp_file)
    CM_port=$(sed -n '5p' $install_temp_file)
    device_dnpath_ip_str=$DN_path

    i=1
    while (( i <= nodeCount ))
    do
        dialog --title "$title_str" --ok-label "下一步" --cancel-label "取消" --form "设置配置文件选项 节点$i" 10 45 3 \
        "主机名" 1 1 "" 1 10 25 0 \
        "主机IP" 2 1 "" 2 10 25 0 2> $install_temp_file$i
        check_dialog_res
        i=$((i+1))
    done

    device_name_str=""
    device_ip_str=""
    i=1
    while (( i <= nodeCount ))
    do
        if [ $i == 1 ]
        then
            device_name_str=$(head -1 $install_temp_file$i)
            device_ip_str=$(sed -n '2p' $install_temp_file$i)
        else
            device_name_str=$device_name_str","$(head -1 $install_temp_file$i)
            device_ip_str=$device_ip_str","$(sed -n '2p' $install_temp_file$i)
            device_dnpath_ip_str=$device_dnpath_ip_str","$(head -1 $install_temp_file$i)","$DN_path
        fi
        i=$((i+1))
    done

    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" > $option_file_name
    echo "<ROOT>" >> $option_file_name
    echo "  <CLUSTER>" >> $option_file_name
    echo "      <PARAM name=\"clusterName\" value=\"gauss\"/>" >> $option_file_name
    echo "      <PARAM name=\"nodeNames\" value=\"$device_name_str\"/>" >> $option_file_name
    echo "      <PARAM name=\"backIp1s\" value=\"$device_ip_str\"/>" >> $option_file_name
    echo "      <PARAM name=\"gaussdbAppPath\" value=\"$install_path/app\"/>" >> $option_file_name
    echo "      <PARAM name=\"gaussdbLogPath\" value=\"$install_path/log\"/>" >> $option_file_name
    echo "      <PARAM name=\"tmpMppdbPath\" value=\"$install_path/tmp\"/>" >> $option_file_name
    echo "      <PARAM name=\"gaussdbToolPath\" value=\"$install_path/om\"/>" >> $option_file_name
    echo "      <PARAM name=\"corePath\" value=\"$install_path/corefile\"/>" >> $option_file_name
    echo "      <PARAM name=\"clusterType\" value=\"single-inst\"/>" >> $option_file_name
    echo "  </CLUSTER>" >> $option_file_name
    echo "  <DEVICELIST>" >> $option_file_name

    i=1
    while (( i <= nodeCount ))
    do
        dev_name=$(head -1 $install_temp_file$i)
        dev_ip=$(sed -n '2p' $install_temp_file$i)
        echo "      <DEVICE sn=\"$dev_name\">" >> $option_file_name
        echo "          <PARAM name=\"name\" value=\"$dev_name\"/>" >> $option_file_name
        echo "          <PARAM name=\"azName\" value=\"AZ1\"/>" >> $option_file_name
        echo "          <PARAM name=\"azPriority\" value=\"1\"/>" >> $option_file_name
        echo "          <PARAM name=\"backIp1\" value=\"$dev_ip\"/>" >> $option_file_name
        echo "          <PARAM name=\"sshIp1\" value=\"$dev_ip\"/>" >> $option_file_name
        echo "          <PARAM name=\"cmDir\" value=\"$install_path/cm\"/>" >> $option_file_name
        if [ $i == 1 ]
        then
            echo "          <PARAM name=\"cmsNum\" value=\"1\"/>" >> $option_file_name
            echo "          <PARAM name=\"cmServerPortBase\" value=\"$CM_port\"/>" >> $option_file_name
            echo "          <PARAM name=\"cmServerlevel\" value=\"1\"/>" >> $option_file_name
            echo "          <PARAM name=\"cmServerListenIp1\" value=\"$device_ip_str\"/>" >> $option_file_name
            echo "          <PARAM name=\"cmServerRelation\" value=\"$device_name_str\"/>" >> $option_file_name
            echo "          <PARAM name=\"dataNum\" value=\"1\"/>" >> $option_file_name
            echo "          <PARAM name=\"dataPortBase\" value=\"$DN_port\"/>" >> $option_file_name
            echo "          <PARAM name=\"dataNode1\" value=\"$device_dnpath_ip_str\"/>" >> $option_file_name
            echo "          <PARAM name=\"dataNode1_syncNum\" value=\"0\"/>" >> $option_file_name
        fi
        echo "      </DEVICE>" >> $option_file_name
        i=$((i+1))
    done
    echo "  </DEVICELIST>" >> $option_file_name
    echo "</ROOT>" >> $option_file_name

    rm -rf $install_temp_file
    i=1
    while (( i <= nodeCount ))
    do
        rm -rf $install_temp_file$i
        i=$((i+1))
    done

    dialog --title "$title_str" --clear --ok-label "继续" --msgbox "新建的配置文件是\n$option_file_name" 10 50
}

function check_env()
{
    command_not_fount_str="command not found"
    check=$(dialog --version 2>&1)
    if [[ $check =~ $command_not_fount_str ]]; then
        echo "This script rely on software dialog, please install dialog"
        exit
    fi
    check=$(expect --version 2>&1)
    if [[ $check =~ $command_not_fount_str ]]; then
        echo "Software expect not found, please install expect then run this script"
        exit
    fi
    user_name=$(whoami)
    if [ "$user_name" != "root" ]; then
        dialog --title "$title_str" --ok-label "退出" --msgbox "请以root的身份运行此安装工具" 10 50
        exit
    fi
}

function set_path()
{
    cd $(dirname $0)
    script_path=$(pwd)
    package_path=$(dirname $script_path)
    if [ $package_path == "/" ]
    then
        dialog --title "$title_str" --ok-label "退出" --msgbox "请不要将安装包置于根目录下" 10 50
        exit
    fi
    evn_file_path=$(dirname $package_path)
    if [ $evn_file_path == "/" ]
    then
        dialog --title "$title_str" --ok-label "退出" --msgbox "安装包与根目录之间至少有两级目录" 10 50
        exit
    fi
    cd $evn_file_path
    if [ ! -d _openGauss_profile ]
    then
        mkdir _openGauss_profile
    fi
    evn_file_path=$evn_file_path/_openGauss_profile

    cd $script_path
}

function set_option()
{
    dialog --title "$title_str" --backtitle "选择配置文件\Z2---->\Zn设置安装用户\Z2---->\Zn\Z1输入密码\Zn" --clear --colors --ok-label "下一步" --cancel-label "取消" --insecure --passwordbox "输入root密码" 0 0 2> $install_temp_file
    check_dialog_res
    root_passwd=$(cat $install_temp_file)
    rm -rf $install_temp_file

    dialog --title "$title_str" --backtitle "选择配置文件\Z2---->\Zn\Z1设置安装用户\Zn\Z2---->\Zn输入密码" --clear --colors --ok-label "下一步" --cancel-label "取消" --form "设置安装用户" 10 35 5 \
        "安装用户" 1 1 "" 1 12 15 0 \
        "所属用户组" 2 1 "" 2 12 15 0 2>$install_temp_file
    check_dialog_res

    i=1
    while read LINE
    do
        case $i in
            1)
                install_user=$LINE
                ;;
            2)
                user_group=$LINE
                ;;
        esac
        i=$((i+1))
    done<$install_temp_file

    dialog --title "$title_str" --backtitle "选择配置文件\Z2---->\Zn设置安装用户\Z2---->\Zn\Z1输入密码\Zn" --clear --colors --ok-label "下一步" --cancel-label "取消" --insecure --passwordbox "输入安装用户密码" 0 0 2> $install_temp_file
    check_dialog_res
    cur_user_passwd=$(cat $install_temp_file)
    rm -rf $install_temp_file

    dialog --title "$title_str" --backtitle "选择配置文件\Z2---->\Zn设置安装用户\Z2---->\Zn\Z1输入密码\Zn" --clear --colors --ok-label "下一步" --cancel-label "取消" --insecure --passwordbox "输入数据库密码" 0 0 2> $install_temp_file
    check_dialog_res
    database_passwd=$(cat $install_temp_file)
    rm -rf $install_temp_file
}

function preinstall_progress_bar()
{
    max_line=72
    while true
    do
        if [ -f "./_visual_install_output_file" ]
        then
            msgstr=$(tail -n 25 _visual_install_output_file)
            line_num=$(echo $(wc -l _visual_install_output_file) | tr -cd [0-9])
            persents=$(awk 'BEGIN{printf "%.0f\n",('$line_num'/'$max_line')*100}')
            echo $persents | dialog --title "预安装中" --gauge "$msgstr" 50 100
            sleep 2
        fi
        ps -p $preinstall_pid >/dev/null 2>&1
        result=$?
        if [ $result != 0 ]
        then
            break
        fi
    done
}

function install_progress_bar()
{
    max_line=50
    while true
    do
        if [ -f "./_visual_install_output_file" ]
        then
            msgstr=$(tail -n 25 _visual_install_output_file)
            line_num=$(echo $(wc -l _visual_install_output_file) | tr -cd [0-9])
            persents=$(awk 'BEGIN{printf "%.0f\n",('$line_num'/'$max_line')*100}')
            echo $persents | dialog --title "正在安装" --gauge "$msgstr" 50 100 
            sleep 2
        fi
        ps -p $install_pid >/dev/null 2>&1
        result=$?
        if [ $result != 0 ]
        then
            break
        fi
    done
}

title_str="openGauss安装"
check_env
set_path

install_temp_file=_temp_file

dialog --title "$title_str" --clear --yes-label "新建" --no-label "选择已有" --yesno "\n  是否新建安装配置文件" 8 30
result=$?
if [ $result != 0 ]
then
    dialog --title "选择配置文件" --backtitle "\Z1选择配置文件\Zn\Z2---->\Zn设置安装用户\Z2---->\Zn输入密码" --clear --colors --ok-label "下一步" --cancel-label "取消" --fselect / 18 100 2> $install_temp_file
    check_dialog_res
    option_file_name=$(cat $install_temp_file)
else
    gen_config_file
fi

set_option

chmod -R 755 $evn_file_path
rm -rf _visual_install_output_file
echo "--root-passwd=$root_passwd --install-user-passwd=$cur_user_passwd " | python3 -u ./gs_preinstall -U $install_user -G $user_group -X $option_file_name --sep-env-file=$evn_file_path/openGauss_install_profile --no-ask-user >_visual_install_output_file 2>&1 &
preinstall_pid=$!

preinstall_progress_bar

wait $preinstall_pid
result=$?
if [ $result != 0 ]
then
    dialog --title "$title_str" --clear --ok-label "查看log" --msgbox "\n       预安装错误" 8 30
    dialog --title "预安装log" --clear --exit-label "继续" --textbox ./_visual_install_output_file 50 100
    dialog --title "$title_str" --ok-label "退出" --msgbox "\n安装log位于$script_path/_visual_install_output_file" 8 50
    exit
fi

usermod -G wheel $install_user
chown -R $install_user:$user_group $evn_file_path
chown -R $install_user:$user_group $package_path

rm -rf _install_pid
rm -rf _install_result
rm -rf _visual_install_output_file
su - $install_user -c "$script_path/visual_install_user.sh $option_file_name $database_passwd" >_install_sh_output 2>&1 &

while true
do
    if [ -f "_install_pid" ]
    then
        break
    fi
done
install_pid=$(cat _install_pid)
echo $install_pid

install_progress_bar

while true
do
    if [ -f "_install_result" ]
    then
        break
    fi
done

result=$(cat _install_result)
if [ $result != 0 ]
then
    dialog --title "openGauss安装" --ok-label "查看log" --msgbox "\n      安装错误" 8 30
    dialog --title "安装log" --clear --exit-label "继续" --textbox ./_visual_install_output_file 50 100
    dialog --title "openGauss安装" --ok-label "退出" --msgbox "\n安装log位于$script_path/_visual_install_output_file" 8 50
    exit
else
    dialog --title "openGauss安装" --ok-label "完成" --msgbox "\n      安装成功" 8 30
fi

echo "export LD_LIBRARY_PATH=\$PGHOME/script/gspylib/clib:\$LD_LIBRARY_PATH" >> ../../_openGauss_profile/openGauss_install_profile